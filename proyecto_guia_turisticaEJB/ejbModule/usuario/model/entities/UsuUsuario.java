package usuario.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the usu_usuario database table.
 * 
 */
@Entity
@Table(name="usu_usuario")
@NamedQuery(name="UsuUsuario.findAll", query="SELECT u FROM UsuUsuario u")
public class UsuUsuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_usuario", unique=true, nullable=false)
	private Integer idUsuario;

	@Column(nullable=false, length=100)
	private String apellidos;

	@Column(name="confirmacion_correo")
	private Boolean confirmacionCorreo;

	@Column(nullable=false, length=100)
	private String contrasenia;

	@Column(nullable=false, length=100)
	private String correo;

	@Column(name="fecha_creacion")
	private Timestamp fechaCreacion;

	@Column(name="fecha_modificacion")
	private Timestamp fechaModificacion;

	@Column(nullable=false, length=100)
	private String nombres;

	//bi-directional many-to-one association to UsuBitacora
	@OneToMany(mappedBy="usuUsuario")
	private List<UsuBitacora> usuBitacoras;

	//bi-directional many-to-one association to UsuUsuarioRol
	@OneToMany(mappedBy="usuUsuario")
	private List<UsuUsuarioRol> usuUsuarioRols;

	public UsuUsuario() {
	}

	public Integer getIdUsuario() {
		return this.idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Boolean getConfirmacionCorreo() {
		return this.confirmacionCorreo;
	}

	public void setConfirmacionCorreo(Boolean confirmacionCorreo) {
		this.confirmacionCorreo = confirmacionCorreo;
	}

	public String getContrasenia() {
		return this.contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	public String getCorreo() {
		return this.correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public Timestamp getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Timestamp fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Timestamp getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Timestamp fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getNombres() {
		return this.nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public List<UsuBitacora> getUsuBitacoras() {
		return this.usuBitacoras;
	}

	public void setUsuBitacoras(List<UsuBitacora> usuBitacoras) {
		this.usuBitacoras = usuBitacoras;
	}

	public UsuBitacora addUsuBitacora(UsuBitacora usuBitacora) {
		getUsuBitacoras().add(usuBitacora);
		usuBitacora.setUsuUsuario(this);

		return usuBitacora;
	}

	public UsuBitacora removeUsuBitacora(UsuBitacora usuBitacora) {
		getUsuBitacoras().remove(usuBitacora);
		usuBitacora.setUsuUsuario(null);

		return usuBitacora;
	}

	public List<UsuUsuarioRol> getUsuUsuarioRols() {
		return this.usuUsuarioRols;
	}

	public void setUsuUsuarioRols(List<UsuUsuarioRol> usuUsuarioRols) {
		this.usuUsuarioRols = usuUsuarioRols;
	}

	public UsuUsuarioRol addUsuUsuarioRol(UsuUsuarioRol usuUsuarioRol) {
		getUsuUsuarioRols().add(usuUsuarioRol);
		usuUsuarioRol.setUsuUsuario(this);

		return usuUsuarioRol;
	}

	public UsuUsuarioRol removeUsuUsuarioRol(UsuUsuarioRol usuUsuarioRol) {
		getUsuUsuarioRols().remove(usuUsuarioRol);
		usuUsuarioRol.setUsuUsuario(null);

		return usuUsuarioRol;
	}

}