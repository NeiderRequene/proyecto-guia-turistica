package multimedia.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the mul_multimedia database table.
 * 
 */
@Entity
@Table(name="mul_multimedia")
@NamedQuery(name="MulMultimedia.findAll", query="SELECT m FROM MulMultimedia m")
public class MulMultimedia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_multimedia", unique=true, nullable=false)
	private Integer idMultimedia;

	@Column(length=2147483647)
	private String descripcion;

	@Column(nullable=false, length=2147483647)
	private String direccion;

	@Column(name="fecha_creacion")
	private Timestamp fechaCreacion;

	@Column(name="fecha_modificacion")
	private Timestamp fechaModificacion;

	@Column(name="id_lugar", nullable=false)
	private Integer idLugar;

	@Column(nullable=false, length=200)
	private String nombre;

	//bi-directional many-to-one association to MulTipoArchivo
	@ManyToOne
	@JoinColumn(name="id_tipo_archivo", nullable=false)
	private MulTipoArchivo mulTipoArchivo;

	public MulMultimedia() {
	}

	public Integer getIdMultimedia() {
		return this.idMultimedia;
	}

	public void setIdMultimedia(Integer idMultimedia) {
		this.idMultimedia = idMultimedia;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Timestamp getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Timestamp fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Timestamp getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Timestamp fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Integer getIdLugar() {
		return this.idLugar;
	}

	public void setIdLugar(Integer idLugar) {
		this.idLugar = idLugar;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public MulTipoArchivo getMulTipoArchivo() {
		return this.mulTipoArchivo;
	}

	public void setMulTipoArchivo(MulTipoArchivo mulTipoArchivo) {
		this.mulTipoArchivo = mulTipoArchivo;
	}

}