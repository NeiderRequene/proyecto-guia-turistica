package lugar.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the lug_lugar database table.
 * 
 */
@Entity
@Table(name="lug_lugar")
@NamedQuery(name="LugLugar.findAll", query="SELECT l FROM LugLugar l")
public class LugLugar implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_lugar", unique=true, nullable=false)
	private Integer idLugar;

	@Column(length=2147483647)
	private String descripcion;

	@Column(length=100)
	private String direccion;

	@Column(name="fecha_creacion")
	private Timestamp fechaCreacion;

	@Column(name="fecha_modificacion")
	private Timestamp fechaModificacion;

	@Column(name="id_division_politica", nullable=false)
	private Integer idDivisionPolitica;

	@Column(name="id_usuario", nullable=false)
	private Integer idUsuario;

	@Column(nullable=false, length=30)
	private String latitud;

	@Column(nullable=false, length=30)
	private String longitud;

	@Column(nullable=false, length=500)
	private String nombre;

	@Column(length=100)
	private String referencia;

	//bi-directional many-to-one association to LugContacto
	@OneToMany(mappedBy="lugLugar")
	private List<LugContacto> lugContactos;

	//bi-directional many-to-one association to LugTipoLugar
	@ManyToOne
	@JoinColumn(name="id_tipo_lugar", nullable=false)
	private LugTipoLugar lugTipoLugar;

	//bi-directional many-to-one association to LugReporte
	@OneToMany(mappedBy="lugLugar")
	private List<LugReporte> lugReportes;

	public LugLugar() {
	}

	public Integer getIdLugar() {
		return this.idLugar;
	}

	public void setIdLugar(Integer idLugar) {
		this.idLugar = idLugar;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Timestamp getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Timestamp fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Timestamp getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Timestamp fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Integer getIdDivisionPolitica() {
		return this.idDivisionPolitica;
	}

	public void setIdDivisionPolitica(Integer idDivisionPolitica) {
		this.idDivisionPolitica = idDivisionPolitica;
	}

	public Integer getIdUsuario() {
		return this.idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getLatitud() {
		return this.latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getLongitud() {
		return this.longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getReferencia() {
		return this.referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public List<LugContacto> getLugContactos() {
		return this.lugContactos;
	}

	public void setLugContactos(List<LugContacto> lugContactos) {
		this.lugContactos = lugContactos;
	}

	public LugContacto addLugContacto(LugContacto lugContacto) {
		getLugContactos().add(lugContacto);
		lugContacto.setLugLugar(this);

		return lugContacto;
	}

	public LugContacto removeLugContacto(LugContacto lugContacto) {
		getLugContactos().remove(lugContacto);
		lugContacto.setLugLugar(null);

		return lugContacto;
	}

	public LugTipoLugar getLugTipoLugar() {
		return this.lugTipoLugar;
	}

	public void setLugTipoLugar(LugTipoLugar lugTipoLugar) {
		this.lugTipoLugar = lugTipoLugar;
	}

	public List<LugReporte> getLugReportes() {
		return this.lugReportes;
	}

	public void setLugReportes(List<LugReporte> lugReportes) {
		this.lugReportes = lugReportes;
	}

	public LugReporte addLugReporte(LugReporte lugReporte) {
		getLugReportes().add(lugReporte);
		lugReporte.setLugLugar(this);

		return lugReporte;
	}

	public LugReporte removeLugReporte(LugReporte lugReporte) {
		getLugReportes().remove(lugReporte);
		lugReporte.setLugLugar(null);

		return lugReporte;
	}

}