package informacion.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the inf_division_politica database table.
 * 
 */
@Entity
@Table(name="inf_division_politica")
@NamedQuery(name="InfDivisionPolitica.findAll", query="SELECT i FROM InfDivisionPolitica i")
public class InfDivisionPolitica implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_division_politica", unique=true, nullable=false)
	private Integer idDivisionPolitica;

	@Column(nullable=false)
	private Long nivel;

	@Column(nullable=false, length=100)
	private String nombre;

	//bi-directional many-to-one association to InfDivisionPolitica
	@ManyToOne
	@JoinColumn(name="id_division_politica_fk")
	private InfDivisionPolitica infDivisionPolitica;

	//bi-directional many-to-one association to InfDivisionPolitica
	@OneToMany(mappedBy="infDivisionPolitica")
	private List<InfDivisionPolitica> infDivisionPoliticas;

	public InfDivisionPolitica() {
	}

	public Integer getIdDivisionPolitica() {
		return this.idDivisionPolitica;
	}

	public void setIdDivisionPolitica(Integer idDivisionPolitica) {
		this.idDivisionPolitica = idDivisionPolitica;
	}

	public Long getNivel() {
		return this.nivel;
	}

	public void setNivel(Long nivel) {
		this.nivel = nivel;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public InfDivisionPolitica getInfDivisionPolitica() {
		return this.infDivisionPolitica;
	}

	public void setInfDivisionPolitica(InfDivisionPolitica infDivisionPolitica) {
		this.infDivisionPolitica = infDivisionPolitica;
	}

	public List<InfDivisionPolitica> getInfDivisionPoliticas() {
		return this.infDivisionPoliticas;
	}

	public void setInfDivisionPoliticas(List<InfDivisionPolitica> infDivisionPoliticas) {
		this.infDivisionPoliticas = infDivisionPoliticas;
	}

	public InfDivisionPolitica addInfDivisionPolitica(InfDivisionPolitica infDivisionPolitica) {
		getInfDivisionPoliticas().add(infDivisionPolitica);
		infDivisionPolitica.setInfDivisionPolitica(this);

		return infDivisionPolitica;
	}

	public InfDivisionPolitica removeInfDivisionPolitica(InfDivisionPolitica infDivisionPolitica) {
		getInfDivisionPoliticas().remove(infDivisionPolitica);
		infDivisionPolitica.setInfDivisionPolitica(null);

		return infDivisionPolitica;
	}

}