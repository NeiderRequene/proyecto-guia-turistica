package multimedia.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the mul_calificacion database table.
 * 
 */
@Entity
@Table(name="mul_calificacion")
@NamedQuery(name="MulCalificacion.findAll", query="SELECT m FROM MulCalificacion m")
public class MulCalificacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_calificacion", unique=true, nullable=false)
	private Integer idCalificacion;

	@Column(name="fecha_creacion")
	private Timestamp fechaCreacion;

	@Column(name="fecha_modificacion")
	private Timestamp fechaModificacion;

	@Column(name="id_lugar", nullable=false)
	private Integer idLugar;

	@Column(name="id_usuario", nullable=false)
	private Integer idUsuario;

	@Column(length=200)
	private String observacion;

	@Column(nullable=false)
	private Long valor;

	public MulCalificacion() {
	}

	public Integer getIdCalificacion() {
		return this.idCalificacion;
	}

	public void setIdCalificacion(Integer idCalificacion) {
		this.idCalificacion = idCalificacion;
	}

	public Timestamp getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Timestamp fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Timestamp getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Timestamp fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Integer getIdLugar() {
		return this.idLugar;
	}

	public void setIdLugar(Integer idLugar) {
		this.idLugar = idLugar;
	}

	public Integer getIdUsuario() {
		return this.idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Long getValor() {
		return this.valor;
	}

	public void setValor(Long valor) {
		this.valor = valor;
	}

}