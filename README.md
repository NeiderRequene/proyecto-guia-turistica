# TurGui (provisional)
Sistema de guía turística. 

### Tomar en cuenta las siguientes consideraciones para importar el proyecto en Eclipse:
- Crear la base de datos **db_turismo** en la base de datos Postgresql con el respaldo ubicado en proyecto_guia_turisticaEJB/docs.
- Insertar los datos ejemplo con el script script_data.sql
- Crear un datasource en Wildfly 14 denominado **db_turismoDS**.
- Crear un datasource en el IDE Eclipse denominado **db_turismoDS**.
