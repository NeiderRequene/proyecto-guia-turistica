package lugar.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the lug_reporte database table.
 * 
 */
@Entity
@Table(name="lug_reporte")
@NamedQuery(name="LugReporte.findAll", query="SELECT l FROM LugReporte l")
public class LugReporte implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_reporte", unique=true, nullable=false)
	private Integer idReporte;

	private Timestamp fecha;

	@Column(name="id_usuario")
	private Integer idUsuario;

	@Column(length=2147483647)
	private String observacion;

	//bi-directional many-to-one association to LugEstadoReporte
	@ManyToOne
	@JoinColumn(name="id_estado_reporte", nullable=false)
	private LugEstadoReporte lugEstadoReporte;

	//bi-directional many-to-one association to LugLugar
	@ManyToOne
	@JoinColumn(name="id_lugar", nullable=false)
	private LugLugar lugLugar;

	public LugReporte() {
	}

	public Integer getIdReporte() {
		return this.idReporte;
	}

	public void setIdReporte(Integer idReporte) {
		this.idReporte = idReporte;
	}

	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public Integer getIdUsuario() {
		return this.idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public LugEstadoReporte getLugEstadoReporte() {
		return this.lugEstadoReporte;
	}

	public void setLugEstadoReporte(LugEstadoReporte lugEstadoReporte) {
		this.lugEstadoReporte = lugEstadoReporte;
	}

	public LugLugar getLugLugar() {
		return this.lugLugar;
	}

	public void setLugLugar(LugLugar lugLugar) {
		this.lugLugar = lugLugar;
	}

}